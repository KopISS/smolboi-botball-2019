Copyright 2019 Mckol <mckol363@gmail.com>

# What is this?

This is code for our small robot (codename smolboi), it was built and programmed in under 4h on the testing day of the competition, tweaks were made during the competition too.

# Why are you releasing this?

A few people wanted to see it, it's bad code that somehow works, don't make stuff like this, do it properly.

# How did it go?

We got nearly no points in seeding but there were only 4 teams from the entire country so we got to the other stage. Then we got to the final and got 2nd place. It was our first time participating btw. Camera code used for choosing the destination suddenly stopped working that day so luck was an important factor, also the other programmer did a good job fixing the other robot on the go. We lost 199 to *I think* 384 in the final round.

# What does this run on?

KIPR Wallaby controller.  
I used `ssh`, `vim` and `scp` for programming this instead of the crappy web IDE. Also `screen` for running it and detaching the terminal before going to the table.  
Also, it uses libwallaby, [docs are here](https://files.kipr.org/wallaby/wallaby_doc/index.html).  