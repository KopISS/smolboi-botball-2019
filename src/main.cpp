#include <kipr/botball.h>
#include "util.hpp"
#include <thread>
#include <signal.h>

// This whole thing isn't good code by any means, it and the robot it ran on were rebuilt the day before the competitition.

// This was the minimal amount of time the robot needed to stop for to get consistent consequent turns
void pause_motors() {
    ao();
    msleep(400);
}

// There were start lights so this waits for them to turn on
void wait_for_start_signal() {
    while(1) {
        if(analog(START) < 100) { 
            break;
        }
        msleep(5);
    }
}

// Checks if the fire marker is detected by the camera, didn't work for shit on the actual competition day
bool fire_far() {
    camera_open_black();
    camera_load_config("zajomc");
    camera_update();
    bool ret = true;
    // Loop was a desperate try at getting it to detect stuff
    for(int i = 0; i > 3; i++) {
    if(get_object_count(0) > 0) {
        ret = false;
    }
    msleep(100);
    }
    camera_close();

    return ret;
}

// Follows the line until center sensor sees white floor
void line_follower_until_white() {
    mav(LEFT_MOTOR, 500);
    mav(RIGHT_MOTOR, 500);
    while(1) {
        if(right_color() != White && left_color() != White) {
            mav(LEFT_MOTOR, 500);
            mav(RIGHT_MOTOR, 500);
        }
        else if(right_color() == White) {
            mav(RIGHT_MOTOR, 750);
            mav(LEFT_MOTOR, 500);
        }
        else if(left_color() == White) {
            mav(RIGHT_MOTOR, 500);
            mav(LEFT_MOTOR, 750);
        }
        if(center_color() == White) {
            ao();
            break;
        }
        msleep(100);
    }
}

// Line follower that runs for n / 10 seconds
void line_follower_until_loops(int n) {
    mav(LEFT_MOTOR, 500);
    mav(RIGHT_MOTOR, 500);
    for(int i = 0; i < n; i++) {
        if(right_color() != White && left_color() != White) {
            mav(LEFT_MOTOR, 500);
            mav(RIGHT_MOTOR, 500);
        }
        else if(right_color() == White) {
            mav(RIGHT_MOTOR, 750);
            mav(LEFT_MOTOR, 500);
        }
        else if(left_color() == White) {
            mav(RIGHT_MOTOR, 500);
            mav(LEFT_MOTOR, 750);
        }
        msleep(100);
    }
}

// Entire way until the potentially burning building aka hospital actually
void initial_ride() {
    bmotor(100);
    msleep(2500);
    while(center_color() == White) {
        msleep(10);
    }
    msleep(500);
    ao();
    lmotor(50);
    rmotor(-50);
    msleep(1100);
    ao();
    bmotor(100);
    msleep(1000);
    line_follower_until_loops(50);
    pause_motors();
    lmotor(-50);
    rmotor(50);
    msleep(2500);
    ao();
    pause_motors();
    line_follower_until_loops(100);
    line_follower_until_white();

}

// The thread that does stuff
void thread_f() {
    msleep(1000);
    initial_ride();
    if(!fire_far()) {
        bmotor(-100);
        msleep(200);
        lmotor(50);
        rmotor(-50);
        msleep(1200);
        ao();
        bmotor(100);
        msleep(700);
        ao();
        lmotor(-50);
        rmotor(50);
        msleep(800);
        bmotor(100);
        msleep(300);
    }
    ao();
}

extern "C" void abort_f(int n) {
    std::terminate();
}

int main() {
    // Call terminate() on every abort. Apparently that causes terminate to be called recursively and eventually a segmentation fault, but stops all threads when ^C is pressed so *shrug*
    signal(SIGABRT, &abort_f);
    wait_for_start_signal();
    // The thread is so that I can kill the program after time (120s) runs out.
    // I thought if main() returns everything gets killed, read on stackoverflow that it's required by the standard, apparently it doesn't here. 
    // Because of that initial assumption the waiting is in main(), as it would be a waste of time to move it.
    // terminate() is everywhere because it kills all threads.
    std::thread t{thread_f};
    msleep(119500);
    std::terminate();
    return 0;
}
