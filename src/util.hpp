#include <kipr/botball.h>

// This is a file that used to have all the sensor abstraction stuff which was quite a bit of code, not that much anymore but also has motor abstractions

// Motor port indexes
const int LEFT_MOTOR = 0;
const int RIGHT_MOTOR = 1;

// Analog port indexes
const int LEFT_LIGHT = 0; 
const int RIGHT_LIGHT = 2; 
const int CENTER_LIGHT = 1;
const int START = 3;

// This just makes everything more readable
enum Light {
    Black=0,
    Gray,
    White,
};

// This takes thresholds as arguments and I have small functions after it for each sensor to avoid unnecessary code duplication
Light analog_color(int n, int gray_th, int white_th) {
    int raw = analog(n);
    if(raw < white_th) {
        return White;
    }
    else if(raw < gray_th) {
        return Gray;
    }
    else {
        return Black;
    }
}

Light left_color() {
    return analog_color(LEFT_LIGHT, 3600, 2100);
}

Light right_color() {
    return analog_color(RIGHT_LIGHT, 3600, 3000);
}

Light center_color() {
    return analog_color(CENTER_LIGHT, 500, 220);
}

// Following are just functions that make using motors faster

void rmotor(int v) {
    motor(RIGHT_MOTOR, v);
}

void lmotor(int v) {
    motor(LEFT_MOTOR, v);
}

void bmotor(int v) {
    motor(LEFT_MOTOR, v);
    motor(RIGHT_MOTOR, v);
} 
